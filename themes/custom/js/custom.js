/* Add your custom template javascript here */

function finnaCustomInit() {
        
    var modules = [
        'resultsOnMap',
    ];

    $.each(modules, function initModule(ind, module) {
        if (typeof finna[module] !== 'undefined') {
          finna[module].init();
        }
    });
}
